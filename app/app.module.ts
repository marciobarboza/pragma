import './rxjs-extensions';
import { NgModule, enableProdMode }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent }   from './app.component';
import { AppRoutingModule }     from './app-routing.module';

import { LoginComponent }     from './pages/login/login.component';
import { RestaurantsComponent }     from './pages/restaurants/restaurants.component';
import { NavigationComponent }     from './pages/shared/navigation.component';
import { RankingComponent }     from './pages/ranking/ranking.component';
import { ConfigurationComponent }     from './pages/configuration/configuration.component';

import { UserService } from './shared/user/user.service';
import { RestaurantService } from './shared/restaurant/restaurant.service';

enableProdMode();

@NgModule({
  providers: [
    UserService,
    RestaurantService
    ],
  imports:      [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      HttpModule
       ],
  declarations: [
    AppComponent,
    LoginComponent,
    RestaurantsComponent,
    NavigationComponent,
    RankingComponent,
    ConfigurationComponent
    ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }