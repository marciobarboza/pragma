# README #

Where go for lunch - Pragma

### What is this repository for? ###

* AngularJS 2
* Typescript
* [Live site](http://pragma.businesscheckin.co/)

### How do I get set up? ###

* # Config endpoint #
### PATH: app/shared/config.ts: ###
    static apiUrl = "http://{my-domain}}/api/";

* # Login #
### POST params: ###
    Some user data for test:
        username: user01
        password: 123456
        ----------------
        username: user02
        password: 123456
        ----------------
        username: user03
        password: 123456
        ----------------
* # Installing AngularJS 2 #
* [AngularJS 2](https://angular.io/docs/ts/latest/quickstart.html)

        