import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../shared/user/user';
import { UserService } from '../../shared/user/user.service';

@Component({
    moduleId: module.id,
    selector: 'my-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
export class LoginComponent {

    user:User;

    constructor(
        private userService: UserService,
        private router: Router) {
            this.user = new User();
        }

    login() {
        if(this.user.username != null && this.user.password) {
            this.userService.login(this.user)
            .subscribe(
                () => this.router.navigate(["/restaurants"]),
                (error) => alert("Unfortunately we could not find your account.")
            );
        } else {
            alert("Please fill up all fields");
        }
    }
}