import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Restaurant } from '../../shared/restaurant/restaurant';
import { RestaurantService } from '../../shared/restaurant/restaurant.service';

@Component({
    moduleId: module.id,
    selector: 'my-ranking',
    templateUrl: 'ranking.component.html',
    styleUrls: ['../shared/restaurant-list.component.css','ranking.component.css'],
    providers: [RestaurantService]
})
export class RankingComponent implements OnInit {
    restaurantList: Array<Restaurant> = [];
    isLoading = false;
    listLoaded = false;

    constructor(private restaurantService: RestaurantService) {}

    ngOnInit() {
        this.isLoading = true;
        this.restaurantService.loadRanking()
            .subscribe(loadedRestaurants => {
                this.restaurantList = loadedRestaurants;
      this.isLoading = false;
      this.listLoaded = true;
    });
    }
}