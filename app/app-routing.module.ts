import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }  from './pages/login/login.component';
import { RestaurantsComponent }  from './pages/restaurants/restaurants.component';
import { RankingComponent }  from './pages/ranking/ranking.component';
import { ConfigurationComponent }  from './pages/configuration/configuration.component';



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: 'restaurants',  component: RestaurantsComponent },
  { path: 'ranking',  component: RankingComponent },
  { path: 'configuration',  component: ConfigurationComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}