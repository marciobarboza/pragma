import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Restaurant } from '../../shared/restaurant/restaurant';
import { RestaurantService } from '../../shared/restaurant/restaurant.service';

@Component({
    moduleId: module.id,
    selector: 'my-restaurants',
    templateUrl: 'restaurants.component.html',
    styleUrls: ['../shared/restaurant-list.component.css','restaurants.component.css'],
    providers: [RestaurantService]
})
export class RestaurantsComponent implements OnInit {
    restaurantList: Array<Restaurant> = [];
    isLoading = false;
    listLoaded = false;

    constructor(private restaurantService: RestaurantService) {}

    ngOnInit() {
        this.isLoading = true;
        this.restaurantService.loadRestaurants()
            .subscribe(loadedRestaurants => {
                this.restaurantList = loadedRestaurants;
        this.isLoading = false;
        this.listLoaded = true;
        });
    }
    vote(restaurant: Restaurant): void {
        this.isLoading = true;
        this.restaurantService.vote(restaurant)
        .subscribe(
            () => this.isLoading = false,
            (error) => alert(error));
    }
}