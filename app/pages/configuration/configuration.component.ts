import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'my-configuration',
    templateUrl: 'configuration.component.html',
    styleUrls: ['configuration.component.css']
})
export class ConfigurationComponent {
    now: string;

    constructor(private router: Router) {
        var date = new Date(localStorage.getItem("now"));
        this.now = date.toISOString();
    }
    save() {
        var date = new Date(this.now);
        localStorage.setItem("now", date.toISOString());
        this.router.navigate(["/restaurants"]);
    }
}