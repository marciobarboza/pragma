import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { User } from "./user";
import { Config } from "../config";

@Injectable()
export class UserService {
    constructor(private http: Http) {}

    login(user: User) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let options = new RequestOptions({ headers: headers });

        return this.http.post(
            Config.apiUrl + "users/login",
            JSON.stringify({
                username: user.username,
                password: user.password
            }),
            options
        )
        .map(response => response.json())
        .do(data => {
            console.log(data);
            if(data.status)
            {
                localStorage.setItem("token", data.access_token);
                localStorage.setItem("user_id", data.data.id);
                var d = new Date();
                localStorage.setItem("now", d.toISOString());
            }
        })
        .catch(this.handleErrors);
    }

    handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));
        return Observable.throw(error);
    }
}