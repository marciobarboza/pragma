import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { Restaurant } from "./restaurant";
import { Config } from "../config";

@Injectable()
export class RestaurantService {

    constructor(private http: Http) {}

    vote(restaurant: Restaurant) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", "Bearer " + localStorage.getItem("token"));
        let options = new RequestOptions({ headers: headers });

        return this.http.post(
            Config.apiUrl + "votes/add",
            JSON.stringify({
                userId: localStorage.getItem("user_id"),
                restaurantId: restaurant.id,
                now: localStorage.getItem("now")
            }),
            options
        )
        .map(response => response.json())
        .do(data => {
            console.log(data);
            if(!data.status)
                alert(data.message);
            else
                alert("Thank you for your vote!")
        })
        .catch(this.handleErrors);
    }

    loadRanking() {
        let headers = new Headers();
        headers.append("Authorization", "Bearer " + localStorage.getItem("token"));

        return this.http.get(Config.apiUrl + "restaurants/ranking?now=" + localStorage.getItem("now"), {
            headers: headers
        })
        .map(res => res.json())
        .map(data => {
            let restaurantList = [];
            data.forEach((restaurant) => {
                restaurantList.push(new Restaurant(restaurant.id, restaurant.name, restaurant.numberOfVotes));
            });
            return restaurantList;
        })
        .catch(this.handleErrors);
    }

    loadRestaurants() {
        let headers = new Headers();
        headers.append("Authorization", "Bearer " + localStorage.getItem("token"));

        return this.http.get(Config.apiUrl + "restaurants?now=" + localStorage.getItem("now"), {
            headers: headers
        })
        .map(res => res.json())
        .map(data => {
            let restaurantList = [];
            data.forEach((restaurant) => {
                restaurantList.push(new Restaurant(restaurant.id, restaurant.name, restaurant.numberOfVotes));
            });
            return restaurantList;
        })
        .catch(this.handleErrors);
    }

    handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));
        return Observable.throw(error);
    }
}