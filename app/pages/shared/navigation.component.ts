import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'my-navigation',
    templateUrl: 'navigation.component.html',
    styleUrls: ['navigation.component.css']
})
export class NavigationComponent {
    constructor(private router: Router) {}

    logOff() {
        localStorage.removeItem("token");
        localStorage.removeItem("user_id");
        localStorage.removeItem("now");

        this.router.navigate(["/login"])
    }
}